const express = require('express')

/* mongoose is a package that allows creation of schemas to model our data
structures*/
/* also has access to a number of methods for manipulating our database */
const mongoose = require('mongoose');
const app = express();
const port = 3001;

//to connect into database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.nyots.mongodb.net/batch127_to-do?retryWrites=true&w=majority",
			{
				useNewUrlParser: true,
				useUnifiedTopology: true

			}
);

//connecting to robo3t
/*
	mongoose.connect("mongodb://localhost:27017/databse name", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);
*/
//set notifications for connection success or failure
//connection to the databse, allows us to handle error when an initial conection is established
let db = mongoose.connection;
//if connection error occured output in the console
//console.error.bind(console)= allows us to see errors in browser and in terminal
db.on("error", console.error.bind(console,"connection error"));
//if connection is successful
db.once("open", () => console.log("We are connected to the cloud database"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Mongoose Schema
//schema determines the structure of the docuuments to be written in the db
//schemas is a blueprint
// use the schema() constructor of the mongoose module to create a new schema object
//the "new" keyword create a new schema

const taskSchema = new mongoose.Schema({
	//Define the fields
	name: String,
	status: {
		type: String,
		//default values are always pending
		default: "pending"
	}

})
// first param is the collection data name
//2nd is the blueprint
const Task = mongoose.model("Task", taskSchema);


//create new task
/*
Business Logic
1. Add functionality to check if there are duplicate taks
	- if the task already exists in the database, we return an error.
	- if the task doesn't exist in the database, we add it in database.
2. the task data will be coming from the request's body
3. create a new task object with a "name" field/property
4. the "status" property does not need to be provided becuase our scheme
	default is to pending upon creation of an object.
*/

app.post("/tasks", (req, res) => {
	//check if there are duplicate tasks
	//findOne() is a mongoose method that acts as find ofmongodb
	Task.findOne({ name: req.body.name}, (err, result) => {
		if (result !== null && result.name == req.body.name ){
			return res.send("dupplicate task found")
		}
		else{
			let newTask = new Task ({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("new task created")
				}
			})
		}
	
	})
})

/*
Business Logic
1. We will retrieve all the documents using the get method
2. If an error is encountered, print the error
3. if no errors are found, send success feed back then return the tasks

*/

app.get('/tasks', (req,res) =>{
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			//if no errors are found
			//status 200 means that everything is ok
			//.json method is to send a JSON format for the response
			// hte returned response is purposefully returned as an object with 
			//the "data" property to mirror the real world complex data structures
			return res.status(201).json({
				data: result

			})
		}
	})
})


/*
Business Logic
1. We will retrieve all the documents using "GET"
2. If an error is encountered, print error
3. If there's no error, send a success status back to the client/postmand and return an array of document



Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.
*/


const userSchema = new mongoose.Schema({
	
	username: String,
	password: String,
	email_address: String,
	phone_number: String,
	age: String

})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	
	User.findOne({ username: req.body.username}, (err, result) => {
		if (result !== null && result.username == req.body.username ){
			return res.send("dupplicate task found")
		}
		else{
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password,
				email_address: req.body.email_address,
				phone_number: req.body.phone_number,
				age: req.body.age
			})

			newUser.save((saveErr, savedUser)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("new User created")
				}
			})
		}
	
	})
})

app.get('/users', (req,res) =>{
	User.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(201).json({
				data: result

			})
		}
	})
})


app.listen(port, ()=> console.log(`server is running at ${port}`));

